

# Get list of files
# Source: https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
from os import listdir
from os.path import isfile, join
mypath = "input/"
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

print( onlyfiles )

for input_file in onlyfiles:  
  # Create output file
  inpath = mypath + input_file
  outpath = "output/" + input_file + ".org"
  outfile = open( outpath, "w" )
  outfile.write( "# -*- mode: org -*- \n" )
  
  print( "INPUT: \"" + inpath + "\", OUTPUT: \"" + outpath + "\"" )
  
  # Read lines from file
  infile = open( inpath, "r" )
  
  enum_type = "- "
  
  for line in infile.readlines():
    print( "line:    \"" + line + "\"" )
    
    # Easy stuff
    line = line.replace( "``", "\"" )
    line = line.replace( "''", "\"" )
    line = line.replace( "~\\\\", "" )
    line = line.replace( "\\\\", "" )
    #line = line.lstrip()
    
    # Remove these lines
    if ( "\\begin{tabular}" in line or "\\end{tabular}"  in line ): continue
    if ( "\\end{itemize}" in line ):                                continue
    if ( "\\begin{itemize}" in line ):
      enum_type = "- "
      continue
    
    # Need to loop while there's still LaTeX to convert
    # \command{contents}:
    print( "{ in line?", "{" in line )
    print( "} in line?", "}" in line )
    print( "\\ in line?", "\\" in line )
    
    while ( "{" in line and "}" in line and "\\" in line ):
      # Find any {
      body_start = line.find( "{" )
      
      # Find any }
      body_end = line.find( "}" )
      contents = line[ body_start+1 : body_end ]
      
      # Find any \ command
      command_start = line.find( "\\" )
      command = line[ command_start+1 : body_start ]
      
      before = line[ command_start : body_end+1 ]
      after = before
      
      if    ( command == "textbf" ):        after = "*" + contents + "*"
      elif  ( command == "textit" ):        after = "/" + contents + "/"
      elif  ( command == "texttt" ):        after = "=" + contents + "="
      elif  ( command == "section" ):       after = "* " + contents
      elif  ( command == "subsection" ):    after = "** " + contents
      elif  ( command == "subsubsection" ): after = "*** " + contents
      elif  ( command == "paragraph" ):     after = "- " + contents + " ::"
      else:                                 after = "# NO_CONVERT: " + contents
      
      print( "ORIGINAL LINE: \"" + line + "\"" )    
      print( "before:        \"" + before + "\"" )    
      print( "after:         \"" + after + "\"" ) 
        
      line = line.replace( before, after )
      
    # while ( line.find( "{" ) and line.find( "}" ) and line.find( "\\" ) ):
    
    # Weird stuff
    command_start = line.find( "\\" )
    command_end = line.find( " ", command_start )
    command = line[ command_start+1 : command_end ]
    before = line[ command_start : command_end ]
    after = before
    
    
    
    
    print()
    print( "--------------------------------------------------------------" )
    
    outfile.write( line )
    
    
    
    if  ( command == "newpage" ):
      after = "#+ATTR_HTML: :class new-page \n#+BEGIN_HTML \n ----- \n#+END_HTML"
    
    elif  ( command == "item" ):
      after = enum_type
    
    # Regions
    elif  ( command == "begin" ): 
      if  ( contents == "verbatim" ):
        after = "#+BEGIN_SRC artist"
    
    elif  ( command == "end" ):
      if ( contents == "verbatim" ):
        after = "#+END_SRC"
    
    print( "ORIGINAL LINE:", line, "\nUPDATED LINE:", line.replace( before, after ), "\n" )    
    line = line.replace( before, after )
    
    
    # \section{Boolean Expressions}
    # \subsection{Introduction}
    # \textbf{boolean expressions}
    # \textit{ ``is it daytime?'' }
    # \begin{center}
    
  outfile.close()
